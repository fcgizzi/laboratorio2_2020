#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
   LISTA DE LAS 50 MEJORES CANCIONES DE SPOTIFY (SIN FECHA)
"""


import json


# Función pasa los datos json
def pasa_json():
    todo = []
    archivo = open('top50.csv')
    # cada dato solicitado lo agraga en la lista "todo"
    for i, linea in enumerate(archivo):
        linea = linea.split(',')
        if i > 0:
            # se agragn los datos en sus respectivas listas
            todo.append(linea[0])
            todo.append(linea[1])
            todo.append(linea[2])
            todo.append(linea[3])
            todo.append(linea[4])
            todo.append(linea[10])
            todo.append(linea[13])
    # la lista "todo" se pasará a un json
    with open("spotify.json", 'w') as file:
        json.dump(todo, file)


# Función busca popularidad
def popular():
    archivo = open('top50.csv')
    popu_temp = []
    popu = []
    for i, linea in enumerate(archivo):
        linea = linea.split(',')
        if i > 0:
            popu_temp.append(linea[13])
    # se agregan los valores de popularidad en un lista temporal
    # se transforman a enteros
    # y se agregan en ptra lista
    for i in popu_temp:
        x = int(i)
        popu.append(x)
    # se ordenan y se invierten
    # ya que necesitamos el valor máximo
    popu.sort()
    popu.reverse()
    most_popu = []
    # ~ print(popu)
    # se agrega el valor más alto en una lista vacía
    for i in popu:
        most_popu = popu[0]
    archivo.close()
    archivo = open('top50.csv')
    ruido = []
    ruido_temp = []
    menos_ruido = []
    for i, linea in enumerate(archivo):
        linea = linea.split(',')
        if i > 0:
            ruido_temp.append(linea[7])
    # se agregan los valores de ruido en una lista temporal
    # se transforman a enteros
    # y se agregan en otra lista
    for i in ruido_temp:
        x = int(i)
        # ~ print(x)
        ruido.append(x)
    # se ordena la lista
    ruido.sort()
    # se busca el valor minimo
    # por ende el indice 0 es el que corresponde
    for i in ruido:
        menos_ruido = ruido[0]
    # ~ print(menos_ruido)
    archivo.close()
    archivo = open('top50.csv')
    # se busca en el archivo los datos que correspondan a la mayor popularidad
    # y al valor mínimo de ruido
    for i, linea in enumerate(archivo):
        linea = linea.split(',')
        if i > 0:
            if int(linea[13]) == most_popu:
                if int(linea[7]) == menos_ruido:
                    # si aplica para ambas condiciones, imprime sus datos
                    print("\n_____________________________________")
                    print("  La canción más popular es:", linea[1])
                    print("  Y está entre las menos ruidosas")
                    print("\n  DATOS:")
                    print("  Artista:", linea[2], "| Canción", linea[1], "| Poularidad:", int(linea[13]), "| Valor ruido:", linea[7])
                # si no se encuentra entre las menos ruidosas imprime:
                elif int(linea[7]) != menos_ruido:
                    print("\n_____________________________________")
                    print("  La canción más popular es:", linea[1])
                    print("  Y no está entre las menos ruidosas")


# Función busca cancniones más lentas
def mas_lenta():
    archivo = open('top50.csv')
    lenta_temp = []
    lenta = []
    for i, linea in enumerate(archivo):
        linea = linea.split(',')
        if i > 0:
            lenta_temp.append(linea[4])
    # se agregan los valores a una lista temporal
    # y se transforman a enteros
    for i in lenta_temp:
        x = int(i)
        lenta.append(x)
    # se ordena
    lenta.sort()
    # ~ baila.reverse()
    # se buscan los valores minimos
    # ya que menor sea el valor será más lenta la canión
    for i in lenta:
        primera = lenta[0]
        segunda = lenta[1]
        tercera = lenta[2]
    # ~ print(primera, segunda, tercera)
    archivo.close()
    archivo = open('top50.csv')
    cancion = []
    # se buscan el el archivos los valores menores
    for i, linea in enumerate(archivo):
        linea = linea.split(',')
        if i > 0:
            # al ser iguales se agregan las cancniones en una lista
            if int(linea[4]) == primera:
                cancion.append(linea[1])
            if int(linea[4]) == segunda:
                cancion.append(linea[1])
            if int(linea[4]) == tercera:
                cancion.append(linea[1])
    # se eliminan los valores repetidos
    song = set(cancion)
    print("\n  3 CANCIONES MÁS LENTAS")
    print("  **********************")
    # imprime las cacniones
    for i in song:
        print("  ", i)
    archivo.close()


# Funció determina que género es más bailable
def más_bailable():
    archivo = open('top50.csv')
    baila_temp = []
    baila = []
    for i, linea in enumerate(archivo):
        linea = linea.split(',')
        if i > 0:
            baila_temp.append(linea[6])
    # se agregan en un alista temporal
    # y se transforman a enteros para facilitar su uso
    for i in baila_temp:
        x = int(i)
        baila.append(x)
    baila.sort()
    baila.reverse()
    # se ordenan y se invierten
    # para encontrar los 3 valores más altos
    for i in baila:
        primera = baila[0]
        segunda = baila[1]
        tercera = baila[2]
    # ~ print(primera, segunda, tercera)
    archivo.close()
    archivo = open('top50.csv')
    genero = []
    # se buscan los datos en el archivo que se igualen a los 3 primeros datos
    for i, linea in enumerate(archivo):
        linea = linea.split(',')
        if i > 0:
            # al ser encontrado se agrega el dato de género en una lista
            if int(linea[6]) == primera:
                genero.append(linea[3])
            if int(linea[6]) == segunda:
                genero.append(linea[3])
            if int(linea[6]) == tercera:
                genero.append(linea[3])
    # se eliminan los datos repetidos
    genre = set(genero)
    print("\n  3 GENEROS MÁS BAILABLES")
    print("  ***********************")
    # imprime los principales generos
    for i in genre:
        print("  ", i)
    archivo.close()


# Función calcula la mediana
def mediana_lista():
    archivo = open('top50.csv')
    ruido = []
    ruido_temp = []
    ordenada = []
    for i, linea in enumerate(archivo):
        linea = linea.split(',')
        if i > 0:
            ruido_temp.append(linea[7])
    # se transforman los datos de ruido a enteros
    # para pacilitar su uso
    for i in ruido_temp:
        x = int(i)
        # ~ print(x)
        ruido.append(x)
    # se ordenan de menor a mayor
    ruido.sort()
    # se suman los valores del medio usando esta fórmula
    suma = ruido[int((len(ruido)/2)-1)] + ruido[int(len(ruido)/2)]
    # se divide e imprime mediana
    mediana = int(suma/2)
    print("___________")
    print("MEDIANA:", mediana, "\n")
    print("  Canciones con este ruido")
    print("  ************************")
    archivo.close()
    archivo = open('top50.csv')
    # se buscan las canción que posean el valor de la mediana en su valor de ruido
    for i, linea in enumerate(archivo):
        linea = linea.split(',')
        if i > 0:
            # cuando la encuentre imprimirá sus datos principales
            if int(linea[7]) == mediana:
                print("  ", linea[0], '.', linea[1], '|', linea[2], '|', linea[3])


# Función cuenta total de artistas en el archivo
def total_artistas(archivo):
    # lista vacía
    artistas = []
    total = []
    # variable en la que se guardará el más repetido
    repetido = ""
    # ciclo que revisa en archivo
    for i, linea in enumerate(archivo):
        linea = linea.split(',')
        if i > 0:
            # se agregan todos los artistas a una lista
            # para evaluar repetidos
            artistas.append(linea[2])
            contador = 0
            # se crea otro ciclo para agregar a lista sin que estén repetidos
            for i in range(len(total)):
                if total[i] == linea[2]:
                    contador += 1
            if contador == 0:
                total.append(linea[2])
    # se cuenta el total de artistas sin repetir
    cont = len(total)
    print("  LISTA DE ARTISTAS")
    print("  *****************")
    num = 1
    for i in total:
        print(" ", num, '-', i)
        num += 1
    print("_____________________")
    print("TOTAL DE ARTISTAS:", cont)
    print("__________________________________________________")
    # se evalúa el artista que se repita más
    for artista in artistas:
        temp = 0
        rep = artistas.count(artista)
        if rep > temp:
            repetido = artista
        # se iguala a la variabel y se imprime
    print("ARTISTA MÁS REPETIDO:", repetido, "(", rep, "canciones )")

# Función menú
if __name__ == "__main__":
    ruido = []
    archivo = open('top50.csv')
    total_artistas(archivo)
    mediana_lista()
    más_bailable()
    mas_lenta()
    popular()
    pasa_json()
    archivo.close()

